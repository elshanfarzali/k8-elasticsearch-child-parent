provider "aws" {      
  shared_credentials_file = "$HOME/.aws/credentials"
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
  region = "eu-west-2"
}