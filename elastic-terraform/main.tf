resource "aws_cloudwatch_log_group" "demo-cloud" {
  name = var.aws_cloudwatch_log_group
}

resource "aws_cloudwatch_log_resource_policy" "demo-resource" {
  policy_name = var.aws_cloudwatch_log_resource_policy

  policy_document = <<CONFIG
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "es.amazonaws.com"
      },
      "Action": [
        "logs:PutLogEvents",
        "logs:PutLogEventsBatch",
        "logs:CreateLogStream"
      ],
      "Resource": "arn:aws:logs:*"
    }
  ]
}
CONFIG
}
data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

resource "aws_elasticsearch_domain" "demo" {
  domain_name = var.domain
  elasticsearch_version = var.es_version

  cluster_config {
    instance_type = var.instance_type
  }

  snapshot_options {
    automated_snapshot_start_hour = 23
  }
  ebs_options {
    ebs_enabled = var.ebs_volume_size > 0 ? true : false
    volume_size = var.ebs_volume_size
    volume_type = var.volume_type
  }
  tags = {
    Domain = "demo-test"
  }
  access_policies = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "es:*",
      "Principal": "*",
      "Effect": "Allow",
      "Resource": "arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/${var.domain}/*",
      "Condition": {
        "IpAddress": {"aws:SourceIp": ["89.219.49.163/32"]}
      }
    }
  ]
}
POLICY
  log_publishing_options {
    cloudwatch_log_group_arn = aws_cloudwatch_log_group.demo-cloud.arn
    log_type                 = "INDEX_SLOW_LOGS"
  }
}