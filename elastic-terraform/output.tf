output "arn" {
    value = aws_elasticsearch_domain.demo.arn
} 
output "domain_id" {
    value = aws_elasticsearch_domain.demo.domain_id
} 
output "domain_name" {
    value = aws_elasticsearch_domain.demo.domain_name
} 
output "endpoint" {
    value = aws_elasticsearch_domain.demo.endpoint
} 
output "kibana_endpoint" {
    value = aws_elasticsearch_domain.demo.kibana_endpoint
}