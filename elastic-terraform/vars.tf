variable "domain" {
    type = string
}
variable "tag_domain" {
    type = string
}
variable "volume_type" {
    type = string
}
variable "ebs_volume_size" {}

variable "AWS_ACCESS_KEY" {
  description = "AWS Access Key"
}
variable "AWS_SECRET_KEY" {
  description = "AWS Secret Key"
}
variable "es_version" {
  type = string
  default = "7.10"
}
variable "instance_type" {
  type = string
}
variable "aws_cloudwatch_log_group" {
  type = string
  default = "demo-cloud"
}
variable "aws_cloudwatch_log_resource_policy" {
  type = string
  default = "demo-resources"
}