variable "cidr_block" {
  description = "CIDR Block for the VPC"
}

variable "private_subnets" {
  description = "VPC subnets for k8s cluster"
}

variable "public_subnets" {
  description = "VPC subnets for load balancer"
}
variable "availability_zones" {
  default = ["eu-west-2a","eu-west-2b"]
  type = list(string)
}

variable "cluster_version" {
  default = ""
}

variable "worker_name" {
  description = "Name of the workers"
}

variable "worker_type" {
  description = "Type of the workers"
}
//-------------------------------------------------------------------------

// Variables for the AWS auth

// Will be set from build env
variable "AWS_ACCESS_KEY" {
  description = "AWS Access Key"
}

// Will be set from build env
variable "AWS_SECRET_KEY" {
  description = "AWS Secret Key"
}
variable "eks_cluster_name" {
  description = "EKS Cluster Name"
}
variable "vpc_cidr" {
  type        = string
  default     = "10.0.0.0/8"
  description = "CIDR for VPC"
}